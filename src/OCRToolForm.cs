namespace Net.TASBot.RetrOCR;

using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;

using BizHawk.Bizware.Graphics;
using BizHawk.Client.Common;
using BizHawk.Client.EmuHawk;
using BizHawk.Common.IOExtensions;
using BizHawk.WinForms.Controls;

#if USE_TESSERACT
using TesserNet;
#endif

public delegate ref T DataGetter<T>();

public sealed class FirstGlyphBBoxPanel: TableLayoutPanel {
	private static readonly Size Smol = new(24, 23);

	private static Button GenArrowButton(string label, EventHandler onClick)
		=> OCRToolForm.GenButton(label, Smol, onClick);

	private readonly LabelEx _lblDimensions = new();

	private Rectangle _prevBBox;

	public FirstGlyphBBoxPanel(DataGetter<GlyphGridMode.Data> getData) {
		for (var y = 0; y < 4; y++) RowStyles.Add(new());
		for (var x = 0; x < 6; x++) ColumnStyles.Add(new());
		Size = new(212, 120);

		Controls.Add(
			GenArrowButton("^", (_, _) => {
				ref var data = ref getData().FirstGlyphBBox;
				data.Y--;
				data.Height++;
			}),
			row: 0, column: 1
		);
		Controls.Add(
			GenArrowButton("<", (_, _) => {
				ref var data = ref getData().FirstGlyphBBox;
				data.X--;
				data.Width++;
			}),
			row: 1, column: 0
		);
		Controls.Add(OCRToolForm.GenLabel("NW"), row: 1, column: 1);
		Controls.Add(
			GenArrowButton(">", (_, _) => {
				ref var data = ref getData().FirstGlyphBBox;
				data.X++;
				data.Width--;
			}),
			row: 1, column: 2
		);
		Controls.Add(
			GenArrowButton("v", (_, _) => {
				ref var data = ref getData().FirstGlyphBBox;
				data.Y++;
				data.Height--;
			}),
			row: 2, column: 1
		);

		Controls.Add(
			GenArrowButton("^", (_, _) => getData().FirstGlyphBBox.Height--),
			row: 1, column: 4
		);
		Controls.Add(
			GenArrowButton("<", (_, _) => getData().FirstGlyphBBox.Width--),
			row: 2, column: 3
		);
		Controls.Add(OCRToolForm.GenLabel("SE"), row: 2, column: 4);
		Controls.Add(
			GenArrowButton(">", (_, _) => getData().FirstGlyphBBox.Width++),
			row: 2, column: 5
		);
		Controls.Add(
			GenArrowButton("v", (_, _) => getData().FirstGlyphBBox.Height++),
			row: 3, column: 4
		);

		Controls.Add(_lblDimensions, row: 0, column: 4);
		SetColumnSpan(_lblDimensions, 2);

		var btnGlyphGridToRowStep = OCRToolForm.GenButton("next", new(48, 23), (_, _) => {
			ref var data = ref getData();
			data.Mode = GlyphGridMode.Step.LineSpacingDnD;
			data.DnD.Start();
			data.DnD.IsLineMode = true;
		});
		Controls.Add(btnGlyphGridToRowStep, row: 3, column: 0);
		SetColumnSpan(btnGlyphGridToRowStep, 2);
	}

	public void Tick(ref GlyphGridMode.Data data) {
		if (data.FirstGlyphBBox == _prevBBox) return;
		_prevBBox = data.FirstGlyphBBox;
		_lblDimensions.Text = data.FirstGlyphBBoxStr;
	}
}

public sealed class GridColumnSpacingPanel: TableLayoutPanel {
	public GridColumnSpacingPanel(DataGetter<GlyphGridMode.Data> getData) {
		for (var y = 0; y < 3; y++) RowStyles.Add(new());
		for (var x = 0; x < 2; x++) ColumnStyles.Add(new());
		Size = new(80, 96);

		Size smol = new(32, 23);
		Controls.Add(
			OCRToolForm.GenButton("-><-", smol, (_, _) => getData().Grid!.ColumnOffset--),
			row: 0, column: 0
		);
		Controls.Add(
			OCRToolForm.GenButton("<-->", smol, (_, _) => getData().Grid!.ColumnOffset++),
			row: 0, column: 1
		);
		Controls.Add(
			OCRToolForm.GenButton("-C", smol, (_, _) => getData().Grid!.ColumnCount--),
			row: 1, column: 0
		);
		Controls.Add(
			OCRToolForm.GenButton("+C", smol, (_, _) => getData().Grid!.ColumnCount++),
			row: 1, column: 1
		);

		var btnGlyphGridToRowStep = OCRToolForm.GenButton("done", new(48, 23), (_, _) => getData().Mode = GlyphGridMode.Step.Done);
		Controls.Add(btnGlyphGridToRowStep, row: 2, column: 0);
		SetColumnSpan(btnGlyphGridToRowStep, 2);
	}
}

public sealed class GridLineSpacingPanel: TableLayoutPanel {
	public GridLineSpacingPanel(DataGetter<GlyphGridMode.Data> getData) {
		for (var y = 0; y < 6; y++) RowStyles.Add(new());
		for (var x = 0; x < 3; x++) ColumnStyles.Add(new());
		Size = new(120, 172);

		Size smol = new(32, 23);
		Controls.Add(OCRToolForm.GenButton("^", smol, (_, _) => {
			ref var data = ref getData();
			data.Grid!.Origin.Y--;
			data.FirstGlyphBBox.Y--;
		}), row: 0, column: 1);
		Controls.Add(OCRToolForm.GenButton("<", smol, (_, _) => {
			ref var data = ref getData();
			data.Grid!.Origin.X--;
			data.FirstGlyphBBox.X--;
		}), row: 1, column: 0);
		Controls.Add(OCRToolForm.GenButton(">", smol, (_, _) => {
			ref var data = ref getData();
			data.Grid!.Origin.X++;
			data.FirstGlyphBBox.X++;
		}), row: 1, column: 2);
		Controls.Add(OCRToolForm.GenButton("v", smol, (_, _) => {
			ref var data = ref getData();
			data.Grid!.Origin.Y++;
			data.FirstGlyphBBox.Y++;
		}), row: 2, column: 1);

		Controls.Add(
			OCRToolForm.GenButton("-R", smol, (_, _) => getData().Grid!.RowCount--),
			row: 3, column: 0
		);
		Controls.Add(
			OCRToolForm.GenButton("-><-", smol, (_, _) => getData().Grid!.RowOffset--),
			row: 3, column: 1
		);
		Controls.Add(
			OCRToolForm.GenButton("c", smol, (_, _) => {
				ref var data = ref getData();
				data.FirstGlyphBBox.Height--;
				data.Grid!.GlyphHeight--;
			}),
			row: 3, column: 2
		);
		Controls.Add(
			OCRToolForm.GenButton("+R", smol, (_, _) => getData().Grid!.RowCount++),
			row: 4, column: 0
		);
		Controls.Add(
			OCRToolForm.GenButton("<-->", smol, (_, _) => getData().Grid!.RowOffset++),
			row: 4, column: 1
		);
		Controls.Add(
			OCRToolForm.GenButton("C", smol, (_, _) => {
				ref var data = ref getData();
				data.FirstGlyphBBox.Height++;
				data.Grid!.GlyphHeight++;
			}),
			row: 4, column: 2
		);

		var btnGlyphGridToRowStep = OCRToolForm.GenButton("next", new(48, 23), (_, _) => getData().Mode = GlyphGridMode.Step.Columns);
		Controls.Add(btnGlyphGridToRowStep, row: 5, column: 0);
		SetColumnSpan(btnGlyphGridToRowStep, 2);
	}
}

/// <remarks>
/// TODO clean up usings before publishing<br/>
/// https://online.easyscreenocr.com/Home/JapaneseOCR
/// </remarks>
[ExternalTool("RetrOCR")]
public sealed class OCRToolForm: ToolFormBase, IExternalToolForm, IToolFormAutoConfig {
	private sealed class GlyphGridTab: ModeTab {
		private readonly GridColumnSpacingPanel _columnSpacingPanel;

		private readonly FirstGlyphBBoxPanel _firstGlyphBBoxPanel;

		private readonly Label _firstGlyphDnDLabel = GenLabel("Drag+drop a box around a glyph in the far left column, NW to SE corner", autopos: false);

		private readonly DataGetter<GlyphGridMode.Data> _getData;

		private readonly GridLineSpacingPanel _lineSpacingPanel;

		private readonly Label _lineSpacingDnDLabel = GenLabel("Drag+drop to mark the bottom of the lines of text\n(the lowest pixel of any glyph, not to be confused with the \"baseline\")", autopos: false);

		private GlyphGridMode.Step _prevMode;

		private readonly Action _selectIdleTab;

		private readonly Action<string> _setDebugText;

		public GlyphGridTab(
			DataGetter<GlyphGridMode.Data> getData,
			Action selectIdleTab,
			Action<string> setDebugText
		): base(new GlyphGridMode()) {
			_getData = getData;
			_prevMode = _getData().Mode;
			_selectIdleTab = selectIdleTab;
			_setDebugText = setDebugText;
			Controls.Add(_firstGlyphDnDLabel);
			Controls.Add(_firstGlyphBBoxPanel = new(getData));
			Controls.Add(_lineSpacingDnDLabel);
			Controls.Add(_lineSpacingPanel = new(getData));
			Controls.Add(_columnSpacingPanel = new(getData));
		}

		public override void Tick(ref ProjectData allData, ApiContainer apis) {
			ModeImpl.Tick(ref allData, apis);
			ref var data = ref allData.GlyphGrid;
			if (data.Mode != _prevMode) {
				_prevMode = data.Mode;
				if (_prevMode is GlyphGridMode.Step.Done) {
					_selectIdleTab();
					return;
				}
				_firstGlyphDnDLabel.Visible = _prevMode is GlyphGridMode.Step.FirstGlyphDnD;
				_firstGlyphBBoxPanel.Visible = _prevMode is GlyphGridMode.Step.FirstGlyph;
				_lineSpacingDnDLabel.Visible = _prevMode is GlyphGridMode.Step.LineSpacingDnD;
				_lineSpacingPanel.Visible = _prevMode is GlyphGridMode.Step.LineSpacing;
				_columnSpacingPanel.Visible = _prevMode is GlyphGridMode.Step.Columns;
			}
			if (_prevMode is GlyphGridMode.Step.FirstGlyph) _firstGlyphBBoxPanel.Tick(ref data);
			_setDebugText(_prevMode.ToString());
		}
	}

	private sealed class IdleTab: ModeTab {
		public IdleTab(): base(new IdleMode()) {
			Controls.Add(GenLabel($"Click \"{Mode.GlyphGrid}\" to get started with a new project", autopos: false));
		}
	}

	private sealed class MappingTab: ModeTab {
		public MappingTab(Func<BitmapBuffer> getScreenshot): base(new MappingMode()) {
			PictureBox CreatePicBox(BitmapBuffer bb, int glyphIndex, Padding margin)
				=> new() {
					BackColor = DragAndDropHelper.DefaultPalette.Glyph,
					BackgroundImage = bb.ToSysdrawingBitmap(), // copying T_T but you're meant to clone the Bitmap/Image anyway
					BackgroundImageLayout = ImageLayout.Center,
					Height = bb.Height + 4,
					Margin = margin,
					Tag = glyphIndex,
					Width = bb.Width + 4,
				};
			List<BitmapBuffer> uniqueGlyphs = new();
			List<char> glyphMap = new(); // this isn't going to work for some Kanji and any other characters outside the BMP...
			const char CHAR_UNMAPPED = '�'; // doesn't exist in the CJK font, but fortunately it's replaced by the tofu that I was going for in the first place
			int MaybeAddGlyph(BitmapBuffer b) {
				var i = uniqueGlyphs.FindIndex(b.SequenceEqual);
				if (i < 0)
				{
					i = uniqueGlyphs.Count; // currently excl., after insertion will be incl.
					uniqueGlyphs.Add(b);
					glyphMap.Add(CHAR_UNMAPPED);
				}
				return i;
			}

			SzButtonEx btnCapture = new() { Text = "capture <--" };
			SingleColumnFLP flpLines = new();
			CombinedKanaOSK oskKana = new() { Enabled = false };
			LocSzSingleRowFLP flpMappings = new() { AutoScroll = true, Enabled = false, Size = new(440, 64) };

			Control? lastFocusedTextbox = null;
			EventHandler setLastFocused = (focusedSender, _) => lastFocusedTextbox = (Control) focusedSender;
			oskKana.CharEntered += (_, c) => {
				if (lastFocusedTextbox is null) return;
				var next = $"{lastFocusedTextbox.Text}{c}".Normalize();
				next = next.Substring(startIndex: next.Length - 1, length: 1);
				lastFocusedTextbox.Text = next;
				glyphMap[(int) lastFocusedTextbox.Tag] = next[0];
			};
			EventHandler jumpToMapping = (clickSender, _) => {
				var glyphIndex = (int) ((Control) clickSender).Tag;
				if (glyphIndex < flpMappings.Controls.Count) {
					var entry = flpMappings.Controls[glyphIndex];
					flpMappings.ScrollControlIntoView(entry);
					entry.Controls[1].Select();
				}
			};
			btnCapture.Click += (_, _) => {
				const int OFFSET_LEFT = 64;
				const int COL_COUNT = 13;
				const int COL_OFFSET = 10;
				const int END_OFFSET_X = OFFSET_LEFT + COL_COUNT * COL_OFFSET;
				const int OFFSET_TOP = 32;
				const int ROW_COUNT = 4;
				const int ROW_OFFSET = 16;
				const int END_OFFSET_Y = OFFSET_TOP + ROW_COUNT * ROW_OFFSET;
				flpLines.Controls.Clear(); // ehh GC can deal with the dead ones
				flpMappings.Controls.Clear(); // ditto
				uniqueGlyphs.Clear();
				glyphMap.Clear();
				var screenshot = getScreenshot();
				for (var y = OFFSET_TOP; y < END_OFFSET_Y; y += ROW_OFFSET) {
					SingleRowFLP flpRow = new();
					for (var x = OFFSET_LEFT; x < END_OFFSET_X; x += COL_OFFSET) {
						var bb = screenshot.Copy(region: new(x: x, y: y, width: COL_OFFSET, height: ROW_OFFSET));
						var picBox = CreatePicBox(bb, MaybeAddGlyph(bb), Padding.Empty);
						picBox.Click += jumpToMapping;
						flpRow.Controls.Add(picBox);
					}
					flpLines.Controls.Add(flpRow);
				}
				for (var i = 0; i < uniqueGlyphs.Count; i++) {
					var glyph = uniqueGlyphs[i];
					SzTextBoxEx txtGlyphMapping = new() {
						Font = KanaPartialOSK.CJKFont,
						MaxLength = 1, // again, not going to like codepoints past U+10000
						Padding = glyph.Width < 24
							? Padding.Empty
							: new(left: (glyph.Width - 24) / 2, top: 0, right: 0, bottom: 0),
						Size = new(24, 24),
						Tag = i,
						Text = CHAR_UNMAPPED.ToString(),
						TextAlign = HorizontalAlignment.Center,
					};
					txtGlyphMapping.GotFocus += setLastFocused;
					flpMappings.Controls.Add(new SingleColumnFLP {
						Controls = {
							CreatePicBox(
								glyph,
								i,
								glyph.Width < 24
									? new(left: (24 - glyph.Width) / 2, top: 0, right: 0, bottom: 0)
									: Padding.Empty
							),
							txtGlyphMapping,
						},
					});
				}
				oskKana.Enabled = true;
				flpMappings.Enabled = true;
			};

			Controls.Add(new SingleColumnFLP { Controls = { flpMappings, oskKana, flpLines, btnCapture } });
		}
	}

	private abstract class ModeTab: TabPage {
		public readonly IMode ModeImpl;

		public ModeTab(IMode impl) {
			Tag = ModeImpl = impl;
			base.Text = impl.ImplOf.ToString();
		}

		public virtual void Tick(ref ProjectData allData, ApiContainer apis)
			=> ModeImpl.Tick(ref allData, apis);
	}

	private sealed class OSKTab: ModeTab {
		public OSKTab(): base(new KanaMode()) {
			SzTextBoxEx txtT11nSaved = new() { Font = KanaPartialOSK.CJKFont, ReadOnly = true, Size = new(64, 23) };
			SzTextBoxEx txtT11nEdit = new() { Font = KanaPartialOSK.CJKFont, Size = new(64, 23) };

			CombinedKanaOSK oskKana = new();
			oskKana.CharEntered += (_, c) => txtT11nEdit.Text += c;

			Controls.Add(new SingleColumnFLP {
				Controls = {
					new SingleRowFLP {
						Controls = {
							txtT11nSaved,
							OCRToolForm.GenButton("<--", new(28, 23), (_, _) => txtT11nSaved.Text = txtT11nEdit.Text),
							txtT11nEdit,
							OCRToolForm.GenLabel("(buttons type at end)"),
						},
					},
					oskKana,
				},
			});
		}
	}

	internal static Button GenButton(string label, Size size, EventHandler onClick) {
		SzButtonEx btn = new() { Size = size, Text = label };
		btn.Click += onClick;
		return btn;
	}

	internal static Label GenLabel(string text, bool autopos = true)
		=> autopos ? new LabelEx { Text = text } : new LocLabelEx { Text = text };

	private bool _formValid = true;

	private readonly Label _lblDebug = new() { AutoSize = true };

	private string? _loadedRomHash = null;

	public ApiContainer? _maybeAPIContainer { get; set; }

	private ProjectData _projectData = default;

	private readonly TabControl _tabs;

#if USE_TESSERACT
	private Tesseract? _tesseract = null;
#endif

	private ApiContainer APIs
		=> _maybeAPIContainer!;

	private ModeTab SelectedTab
		=> (ModeTab) _tabs.SelectedTab;

	protected override string WindowTitleStatic
		=> "RetrOCR";

	public OCRToolForm() {
		Action selectIdleTab = () => _tabs!.SelectedIndex = 0;
		Action<string> setDebugText = s => _lblDebug.Text = s;
		_tabs = new() {
			Anchor = AnchorStyles.Left | AnchorStyles.Top | AnchorStyles.Right | AnchorStyles.Bottom,
			Appearance = TabAppearance.Buttons,
			Controls = { //TODO gen? I guess it won't matter once the list is nailed down
				new IdleTab(),
				new GlyphGridTab(() => ref _projectData.GlyphGrid, selectIdleTab, setDebugText),
				new MappingTab(() => this.MainForm.MakeScreenshotImage()),
				new OSKTab(),
			},
			Location = new(0, 24),
			Size = new(472, 296),
		};
		TabControlCancelEventHandler selectingHandler = (_, selectingArgs) => {
			if (selectingArgs.Action is TabControlAction.Selecting) {
				((ModeTab) selectingArgs.TabPage).ModeImpl.Setup(ref _projectData, APIs);
				return;
			}
			if (AskSaveChanges()) APIs.Gui.WithEmuSurface(() => APIs.Gui.ClearGraphics());
			else selectingArgs.Cancel = true;
		};
		_tabs.Deselecting += selectingHandler;
		_tabs.Selecting += selectingHandler;

		ClientSize = new(480, 320);
		SuspendLayout();
		this.AddDummyMenubar();
		Controls.Add(_lblDebug);
		Controls.Add(_tabs);
		ResumeLayout(performLayout: false);
		PerformLayout();
	}

	public override bool AskSaveChanges()
		=> SelectedTab.ModeImpl.PromptToSave(ref _projectData, this);

	protected override void Dispose(bool disposing) {
		if (!_formValid) return;
		_formValid = false;
		_ = AskSaveChanges();
#if USE_TESSERACT
		_tesseract?.Dispose();
#endif
		base.Dispose(disposing: disposing);
		Tools.Close<OCRToolForm>(); // don't want to know why, but it seems child controls were getting disposed while ToolManager still had a reference
	}

	public override void Restart() {
		var gi = APIs.Emulation.GetGameInfo();
		if (gi?.Hash != _loadedRomHash) {
			_ = AskSaveChanges();
			_loadedRomHash = gi?.Hash;
//			if (NoGoodSysIDs.Contains(gi?.SystemId)) ;
		}
#if USE_TESSERACT
		_tesseract?.Dispose();
		_tesseract = new(options => {
			options.Language = "jpn";
			options.PageSegmentation = PageSegmentation.Character;
		});
		var result = _tesseract.Read(Image.FromFile("/home/yoshi/Downloads/test.png"));
		Console.WriteLine(result);
		_lblDebug.Text = $"{result.Length} {result}";
#endif
	}

	protected override void UpdateAfter() {
		if (SelectedTab.ModeImpl.ImplOf is not Mode.GlyphGrid) _lblDebug.Text = "N/A";
		SelectedTab.Tick(ref _projectData, APIs);
	}
}
