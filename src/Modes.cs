namespace Net.TASBot.RetrOCR;

using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

using BizHawk.Client.Common;
using BizHawk.Client.EmuHawk;
using BizHawk.Common.IOExtensions;
using BizHawk.WinForms.Controls;

public struct DragAndDropHelper {
	public enum EState {
		Done = 0,
		Endpoint = 1,
		Origin = 2,
		Waiting = 3,
	}

	private static void NoOp(IGuiApi gui) {}

	public static readonly UIPalette DefaultPalette = new(Color.Magenta, Color.Yellow);

	public Rectangle BBox
		=> new(x: Origin.X, y: Origin.Y, width: Endpoint.X - Origin.X, height: Endpoint.Y - Origin.Y);

	public Point Endpoint;

	/// <remarks>may be set by caller immediately after <see cref="Start"/></remarks>
	public bool IsLineMode;

	public Point Origin;

	public EState State;

	public void Start() {
		if (State is EState.Waiting) return;
		if (State is not EState.Done) throw new InvalidOperationException("already in progress");
		State = EState.Waiting;
		IsLineMode = false;
	}

	public Action<IGuiApi> Tick(IInputApi input, UIPalette palette) {
		static Point ReadMousePos(IReadOnlyDictionary<string, object> dict)
			=> new((int) dict["X"], (int) dict["Y"]);
		//TODO read EmuClient.Buffer{Width,Height}
		const int SCREEN_WIDTH = 256;
		const int SCREEN_HEIGHT = 224;
		static bool IsOnScreen(in Point p)
			=> p.X is > 0 and < SCREEN_WIDTH && p.Y is > 0 and < SCREEN_HEIGHT;
		static void HRule(int y, Color c, IGuiApi gui)
			=> gui.DrawLine(x1: 0, y1: y, x2: SCREEN_WIDTH, y2: y, c);
		switch (State) {
			case EState.Waiting: // drag+drop state is always entered by button (or hotkey maybe), so this step exists to ignore that click
				if (input.GetMouse()["Left"] is false) State = EState.Origin;
				break;
			case EState.Origin:
				var mouse = input.GetMouse();
				var origin = ReadMousePos(mouse);
				if (mouse["Left"] is false) {
					return IsLineMode
						? gui => HRule(origin.Y, palette.CursorPrimary, gui)
						: gui => gui.DrawPointSel(origin, isNW: true, palette.CursorPrimary);
				}
				// else mouse pressed
				if (IsOnScreen(in origin)) {
					Origin = origin;
					State = EState.Endpoint;
				} else {
					State = EState.Waiting; // cancel and try again (note this preserves IsLineMode!)
				}
				break;
			case EState.Endpoint:
				var mouse1 = input.GetMouse();
				var endpoint = ReadMousePos(mouse1);
				if (mouse1["Left"] is true) {
					var origin1 = Origin;
					return IsLineMode
						? gui => {
							var offset = endpoint.Y - origin1.Y;
							for (var i = 1; i <= 3; i++) HRule(origin1.Y + i * offset, palette.CursorSecondary, gui);
							HRule(origin1.Y, palette.CursorPrimary, gui);
						}
						: gui => {
							gui.DrawAxis(origin1, size: 5, palette.CursorPrimary);
							gui.DrawPointSel(endpoint, isNW: false, palette.CursorSecondary);
						};
				}
				// else mouse released
				if (IsOnScreen(in endpoint)) {
					Endpoint = endpoint;
					State = EState.Done;
				} else {
					State = EState.Origin; // cancel and try again (skip Waiting, the mouse state is definitely released at this point)
				}
				break;
			case EState.Done:
			default:
				break;
		}
		return NoOp;
	}
}

public sealed class GlyphGridMode: IMode {
	public struct Data {
		public DragAndDropHelper DnD;

		public Rectangle FirstGlyphBBox;

		public readonly string FirstGlyphBBoxStr
			=> $"{FirstGlyphBBox.Width}x{FirstGlyphBBox.Height}@{FirstGlyphBBox.X},{FirstGlyphBBox.Y}";

		public GlyphPositions.Grid? Grid;

		public Step Mode;
	}

	public enum Step: byte {
		Done = 0,
		Columns = 1,
		LineSpacing = 2,
		LineSpacingDnD = 3,
		FirstGlyph = 4,
		FirstGlyphDnD = 5,
	}

	private static void TickImpl(ref Data data, IGuiApi gui, IInputApi input, UIPalette palette) {
		void DrawFirstGlyphBBox(ref Rectangle bbox)
			=> gui.DrawBox(x: bbox.X, y: bbox.Y, x2: bbox.Right, y2: bbox.Bottom, line: palette.Glyph, background: palette.Glyph);
		switch (data.Mode) {
			case Step.FirstGlyphDnD:
				// D+D helper init'd in Setup
				var dndDraws = data.DnD.Tick(input, palette);
				gui.WithEmuSurface(() => dndDraws(gui));
				if (data.DnD.State is DragAndDropHelper.EState.Done) {
					data.FirstGlyphBBox = data.DnD.BBox;
					data.Mode = Step.FirstGlyph;
				}
				break;
			case Step.FirstGlyph:
				var bbox = data.FirstGlyphBBox;
				gui.WithEmuSurface(() => DrawFirstGlyphBBox(ref bbox));
				break;
				// button advances
			case Step.LineSpacingDnD:
				var bbox1 = data.FirstGlyphBBox;
				// D+D helper init'd when advancing step
				var dndDraws1 = data.DnD.Tick(input, palette);
				gui.WithEmuSurface(() => {
					DrawFirstGlyphBBox(ref bbox1);
					dndDraws1(gui);
				});
				if (data.DnD.State is DragAndDropHelper.EState.Done) {
					data.Grid = new() {
						ColumnCount = 12,
						ColumnOffset = bbox1.Width + 2,
						GlyphHeight = bbox1.Height,
						GlyphWidth = bbox1.Width,
						Origin = new(bbox1.X, data.DnD.Origin.Y - bbox1.Height),
						RowCount = 2,
						RowOffset = data.DnD.Endpoint.Y - data.DnD.Origin.Y,
					};
					data.FirstGlyphBBox.Y = data.Grid.Origin.Y;
					data.Mode = Step.LineSpacing;
				}
				break;
			case Step.LineSpacing:
				var grid = data.Grid!;
				var bbox2 = data.FirstGlyphBBox;
				gui.WithEmuSurface(() => {
					var width = grid.ColumnCount * grid.GlyphWidth;
					for (
						int y = grid.Origin.Y, yEndpointExcl = y + grid.RowCount * grid.RowOffset;
						y < yEndpointExcl;
						y += grid.RowOffset
					) {
						gui.DrawBox(
							x: grid.Origin.X,
							y: y,
							x2: grid.Origin.X + width,
							y2: y + grid.GlyphHeight,
							line: palette.Row,
							background: palette.Row
						);
					}
					DrawFirstGlyphBBox(ref bbox2);
				});
				break;
				// button advances
			case Step.Columns:
				var grid1 = data.Grid!;
				gui.WithEmuSurface(() => {
					foreach (var box in grid1) {
						gui.DrawBox(x: box.X, y: box.Y, x2: box.Right, y2: box.Bottom, line: palette.Row, background: palette.Row);
					}
				});
				break;
				// button advances
			case Step.Done:
				break;
		}
	}

	public Mode ImplOf
		=> Mode.GlyphGrid;

	public bool PromptToSave(ref ProjectData data, IDialogParent dialogParent) {
//		data.SavedGrids.Add(data.GlyphGrid);
		return true;
	}

	public void Setup(ref ProjectData data, ApiContainer apis) {
//		apis.EmuClient.Pause(); // forgot you can't draw while paused...
		data.GlyphGrid.Mode = Step.FirstGlyphDnD;
		data.GlyphGrid.DnD = default;
		data.GlyphGrid.DnD.Start(); // first step is to drag+drop
	}

	public void Tick(ref ProjectData data, ApiContainer apis)
		=> TickImpl(ref data.GlyphGrid, apis.Gui, apis.Input, DragAndDropHelper.DefaultPalette);
}

public interface GlyphPositions: IEnumerable<Rectangle> {
	public sealed class Grid: Rows {
		public static Grid Parse(ReadOnlySpan<char> s) {
			var i = s.IndexOf('+');
			//TODO
			return new();
		}

		public int ColumnCount;

		/// <remarks>must incl. <see cref="GlyphWidth"/></remarks>
		public int ColumnOffset;

		public int GlyphWidth;

//		public Grid(): base() {}

		public override IEnumerator<Rectangle> GetEnumerator() {
			for (int y = Origin.Y, yEndpointExcl = y + RowCount * RowOffset; y < yEndpointExcl; y += RowOffset) {
				for (int x = Origin.X, xEndpointExcl = x + ColumnCount * ColumnOffset; x < xEndpointExcl; x += ColumnOffset) {
					yield return new(x: x, y: y, width: GlyphWidth, height: GlyphHeight);
				}
			}
		}

		public override string ToString()
			=> $"{GlyphWidth}+{ColumnOffset - GlyphWidth}x{GlyphHeight}+{RowOffset - GlyphHeight}@{Origin.X},{Origin.Y}#{ColumnCount}x{RowCount}";
	}

	public abstract class Rows: GlyphPositions {
		public int GlyphHeight;

		public Point Origin;

		public int RowCount;

		/// <remarks>must incl. <see cref="GlyphHeight"/></remarks>
		public int RowOffset;

		public abstract IEnumerator<Rectangle> GetEnumerator();

		IEnumerator IEnumerable.GetEnumerator()
			=> GetEnumerator();
	}
}

public sealed class IdleMode: IMode {
	public Mode ImplOf
		=> Mode.Idle;

	public bool PromptToSave(ref ProjectData data, IDialogParent dialogParent)
		=> true;

	public void Setup(ref ProjectData data, ApiContainer apis) {}

	public void Tick(ref ProjectData data, ApiContainer apis) {}
}

public interface IMode {
	public Mode ImplOf { get; }

	public bool PromptToSave(ref ProjectData data, IDialogParent dialogParent);

	public void Setup(ref ProjectData data, ApiContainer apis);

	public void Tick(ref ProjectData data, ApiContainer apis);
}

public sealed class KanaMode: IMode {
	public struct Data {}

	public Mode ImplOf
		=> Mode.Kana;

	public bool PromptToSave(ref ProjectData data, IDialogParent dialogParent)
		=> true;

	public void Setup(ref ProjectData data, ApiContainer apis) {}

	public void Tick(ref ProjectData data, ApiContainer apis) {}
}

public sealed class MappingMode: IMode {
	public struct Data {}

	public Mode ImplOf
		=> Mode.Mapping;

	public bool PromptToSave(ref ProjectData data, IDialogParent dialogParent)
		=> true;

	public void Setup(ref ProjectData data, ApiContainer apis) {}

	public void Tick(ref ProjectData data, ApiContainer apis) {}
}

public enum Mode: byte {
	Idle = 0,
	GlyphGrid = 1,
	Kana = 2,
	Mapping = 3,
}

public struct ProjectData {
	private IList<GlyphGridMode.Data> _savedGrids;

	public GlyphGridMode.Data GlyphGrid;

	public IList<GlyphGridMode.Data> SavedGrids
		=> _savedGrids ??= new List<GlyphGridMode.Data>(); //TODO name them I guess

	public KanaMode.Data Kana;
}

public readonly struct UIPalette {
	public readonly Color CursorPrimary;

	public readonly Color CursorSecondary;

	public readonly Color Glyph;

	public readonly Color Row;

	public UIPalette(in Color primary, in Color secondary) {
		CursorPrimary = primary;
		CursorSecondary = secondary;
		//TODO calc from primary and secondary?
		Glyph = Color.FromArgb(0x9F, 0xFF, 0x00, 0xFF);
		Row = Color.FromArgb(0x9F, 0xFF, 0xFF, 0x00);
	}
}
