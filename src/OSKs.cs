namespace Net.TASBot.RetrOCR;

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

using BizHawk.Common;
using BizHawk.WinForms.Controls;

/// <param name="c">may be a combining character; do your own normalisation if necessary</param>
public delegate void CharEnteredEventHandler(Control sender, char c);

public delegate void ScriptChangedEventHandler(CombinedKanaOSK sender, bool isKatakana);

public sealed class CombinedKanaOSK: SingleColumnFLP {
	private bool _isKatakana;

	public bool IsKatakana {
		get => _isKatakana;
		set {
			_isKatakana = value;
			ScriptChanged?.Invoke(this, _isKatakana);
		}
	}

	public CombinedKanaOSK() {
		Button GenExtraKey(char c, string? label = null, bool compact = true)
			=> KanaPartialOSK.GenKey(
				label ?? c.ToString(),
				(_, _) => CharEntered?.Invoke(this, c),
				compact: compact
			);

		var hiragana = KanaPartialOSK.CreateHiragana(this);
		hiragana.CharEntered += (sender, c) => CharEntered?.Invoke(sender, c);
		Controls.Add(hiragana);
		var katakana = KanaPartialOSK.CreateKatakana(this);
		katakana.Visible = false;
		katakana.CharEntered += (sender, c) => CharEntered?.Invoke(sender, c);
		Controls.Add(katakana);
		ScriptChanged += (_, b) => {
			hiragana.Visible = !b;
			katakana.Visible = b;
		};

		SzButtonEx btnSwapScript = new() {
			Font = KanaPartialOSK.CJKFont,
			Size = new(80, 23),
			Text = "かな<-->カナ\u00A0", // that's an NBSP--without it, Mono refuses to render the 'ナ'
		};
		btnSwapScript.Click += (_, _) => IsKatakana = !IsKatakana;
		SingleRowFLP flpPunc = new() {
			Controls = {
				GenExtraKey('\u3000', "␣"),
				GenExtraKey('・'),
				GenExtraKey('ー'),
				GenExtraKey('、'),
				GenExtraKey('。'),
			},
		};
		Padding puncMargin = new(top: 4, right: 0, bottom: 0, left: 4);
		((FlowLayoutPanel) flpPunc).Margin = puncMargin;
		Controls.Add(new SingleRowFLP {
			Controls = {
				btnSwapScript,
				GenExtraKey('\u3099', "゛", compact: false),
				GenExtraKey('\u309A', "゜", compact: false),
				flpPunc,
			},
		});

		SingleRowFLP flpExtraPunc = new() {
			Controls = {
#if true
				GenExtraKey('！'),
				GenExtraKey('？'),
#else // variant selector renders as tofu under Mono (it's meant to make the previous fullwidth char left-aligned)
				GenExtraKey('！', "！\uFE00"),
				GenExtraKey('？', "？\uFE00"),
#endif
				GenExtraKey('「'),
				GenExtraKey('」'),
				GenExtraKey('〜'),
				GenExtraKey('￥'),
			},
		};
		((FlowLayoutPanel) flpExtraPunc).Margin = puncMargin;
		Controls.Add(flpExtraPunc);
	}

	/// <seealso cref="CharEnteredEventHandler"/>
	public event CharEnteredEventHandler? CharEntered;

	public event ScriptChangedEventHandler? ScriptChanged;
}

public sealed class KanaPartialOSK: TableLayoutPanel {
	public static readonly string CJKFontName = OSTailoredCode.IsUnixHost ? "Noto Sans CJK JP" : "Microsoft YaHei";

	internal static readonly Font CJKFont = new(CJKFontName, 8.25f);

	internal static readonly Font CJKFontBig = new(CJKFontName, 10.0f);

	private static readonly char[] Hiragana = {
		'ん', 'わ', 'ら', 'や', 'ま', 'は', 'な', 'た', 'さ', 'か', 'あ', 'ぁ', 'ゃ',
		'.', '.', 'り', '.', 'み', 'ひ', 'に', 'ち', 'し', 'き', 'い', 'ぃ', '.',
		'っ', '.', 'る', 'ゆ', 'む', 'ふ', 'ぬ', 'つ', 'す', 'く', 'う', 'ぅ', 'ゅ',
		'.', '.', 'れ', '.', 'め', 'へ', 'ね', 'て', 'せ', 'け', 'え', 'ぇ', '.',
		'ゝ', 'を', 'ろ', 'よ', 'も', 'ほ', 'の', 'と', 'そ', 'こ', 'お', 'ぉ', 'ょ',
	};

	private static readonly char[] Katakana = {
		'ン', 'ワ', 'ラ', 'ヤ', 'マ', 'ハ', 'ナ', 'タ', 'サ', 'カ', 'ア', 'ァ', 'ャ',
		'.', '.', 'リ', '.', 'ミ', 'ヒ', 'ニ', 'チ', 'シ', 'キ', 'イ', 'ィ', '.',
		'ッ', '.', 'ル', 'ユ', 'ム', 'フ', 'ヌ', 'ツ', 'ス', 'ク', 'ウ', 'ゥ', 'ュ',
		'.', '.', 'レ', '.', 'メ', 'ヘ', 'ネ', 'テ', 'セ', 'ケ', 'エ', 'ェ', '.',
		'ヽ', 'ヲ', 'ロ', 'ヨ', 'モ', 'ホ', 'ノ', 'ト', 'ソ', 'コ', 'オ', 'ォ', 'ョ',
	};

	private static readonly Padding KeyPadding = new(top: -5, right: 0, bottom: 0, left: -4);

#if true // Kyros said this is correct, and I couldn't quickly find evidence otherwise, so until I consult a native speaker (which I should) they can glare at me from the right
	private static readonly Padding KeyPaddingSmol = new(top: 0, right: 0, bottom: 0, left: 0);
#else
	private static readonly Padding KeyPaddingSmol = new(top: 0, right: 0, bottom: 0, left: -8);
#endif

	private static readonly Size KeySize = new(24, 23);

	private static readonly Size KeySizeCompact = new(21, 21);

	private static readonly IReadOnlyDictionary<char, string> SmolMap = new Dictionary<char, string> {
		['ぁ'] = "あ", ['ぃ'] = "い", ['ぅ'] = "う", ['ぇ'] = "え", ['ぉ'] = "お",
		['っ'] = "つ",
		['ゃ'] = "や", ['ゅ'] = "ゆ", ['ょ'] = "よ",
		['ァ'] = "ア", ['ィ'] = "イ", ['ゥ'] = "ウ", ['ェ'] = "エ", ['ォ'] = "オ",
		['ッ'] = "ツ",
		['ャ'] = "ヤ", ['ュ'] = "ユ", ['ョ'] = "ヨ",
	};

	public static KanaPartialOSK CreateHiragana(Control? parent)
		=> new(Hiragana, parent);

	public static KanaPartialOSK CreateKatakana(Control? parent)
		=> new(Katakana, parent);

	internal static Button GenKey(string label, EventHandler onClick, bool compact = true, bool smol = false) {
		SzButtonEx btn = new() {
			Font = smol ? CJKFont : CJKFontBig,
			Padding = smol ? KeyPaddingSmol : KeyPadding,
			Size = compact ? KeySizeCompact : KeySize,
			Text = label,
		};
		if (compact) btn.Margin = Padding.Empty;
		btn.Click += onClick;
		return btn;
	}

	private readonly Control _parent;

	private KanaPartialOSK(char[] keycaps, Control? parent) {
		Button GenSimpleKey(char c, string? label = null)
			=> GenKey(label ?? c.ToString(), (_, _) => CharEntered?.Invoke(_parent, c), smol: label is not null);
		_parent = parent ?? this;
		Size = new(280, 112);
		const int ROW_COUNT = 5;
		const int COL_COUNT = 13;
		if (keycaps.Length is not ROW_COUNT * COL_COUNT) throw new ArgumentException(paramName: nameof(keycaps), message: "wrong size");
		for (var y = 0; y < ROW_COUNT; y++) RowStyles.Add(new());
		for (var x = 0; x < COL_COUNT; x++) ColumnStyles.Add(new());
		for (var y = 0; y < ROW_COUNT; y++) for (var x = 0; x < COL_COUNT; x++) {
			var keycap = keycaps[y * COL_COUNT + x];
			if (keycap is '.') continue;
			if (SmolMap.TryGetValue(keycap, out var beeg)) {
				// we're gonna make them big to make them smaller
				var btn = GenSimpleKey(keycap, beeg);
				Controls.Add(btn, row: y, column: x);
			} else {
				Controls.Add(GenSimpleKey(keycap), row: y, column: x);
			}
		}
	}

	/// <seealso cref="CharEnteredEventHandler"/>
	public event CharEnteredEventHandler? CharEntered;
}
