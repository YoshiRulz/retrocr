{ pkgs ? import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/24.05.tar.gz") {}
, lib ? pkgs.lib
, writeShellScriptBin ? pkgs.writeShellScriptBin
, dotnet-sdk ? pkgs.dotnet-sdk_8
, emuhawk ? (import ../../BizHawk {}).emuhawk-latest-bin
, kate ? pkgs.kate
, tesseract ? pkgs.tesseract4
}: pkgs.mkShell {
	packages = lib.attrValues {
#		inherit dotnet-sdk kate;
		loc = writeShellScriptBin "loc" ''
			printf 'Project LOC: %s\n' "$(cat *.cs | wc -l)"
		'';
	};
	shellHook = let
		tesseractRoot = lib.getOutput "out" tesseract;
	in ''
		b() {
			# nothing about this works; deal with it
			dotnet build \
				&& LD_LIBRARY_PATH="$LD_LIBRARY_PATH:${tesseractRoot}/lib" TESSDATA_PREFIX="${tesseractRoot}/share/tessdata" \
					/nix/store/31kzdydsdwjka613qla7ciki1xmmyx59-emuhawk-mono-wrapper \
					--open-ext-tool-dll=RetrOCR "$@"
		}
		b1() {
			if [ "$XDG_DATA_HOME" ]; then
				BIZHAWK_DATA_HOME="$XDG_DATA_HOME"
			else
				BIZHAWK_DATA_HOME="$HOME/.local/share"
			fi
			BIZHAWK_DATA_HOME="$BIZHAWK_DATA_HOME/emuhawk-monort-${emuhawk.hawkSourceInfo.version}/" \
			BIZHAWK_HOME="${emuhawk.assemblies}/" \
			dotnet build \
				&& LD_LIBRARY_PATH="$LD_LIBRARY_PATH:${tesseractRoot}/lib" TESSDATA_PREFIX="${tesseractRoot}/share/tessdata" \
					"${lib.getBin emuhawk}/bin/emuhawk-monort-${emuhawk.hawkSourceInfo.version}" \
					--open-ext-tool-dll=RetrOCR "$@"
		}
	'';
}
