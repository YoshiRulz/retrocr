namespace Net.TASBot.RetrOCR;

using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;

using BizHawk.Client.Common;
using BizHawk.Client.EmuHawk;
using BizHawk.Common.IOExtensions;
using BizHawk.WinForms.Controls;

internal static class Extensions {
	public static void AddDummyMenubar(this Form form)
		=> form.Controls.Add(new MenuStrip {
			Items = { new ToolStripMenuItem("Settings") },
			Visible = false,
		});

	public static void AddRange(this Control.ControlCollection collection, IEnumerable<Control> controls)
		=> collection.AddRange(controls.ToArray());

	public static void DrawAxis(this IGuiApi gui, Point pos, int size, Color stroke)
		=> gui.DrawAxis(x: pos.X, y: pos.Y, size: size, stroke);

	public static void DrawPointSel(this IGuiApi gui, Point pos, bool isNW, Color stroke)
		=> gui.DrawPie(
			x: pos.X - 7, y: pos.Y - 7,
			width: 13, height: 13,
			startangle: isNW ? 90 : 270, sweepangle: 270,
			line: stroke
		);

	public static void WithEmuSurface(this IGuiApi gui, Action action)
		=> gui.WithSurface(DisplaySurfaceID.EmuCore, action);
}
